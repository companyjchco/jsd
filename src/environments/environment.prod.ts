export const environment = {
  production: true,

  api_backend: "http://localhost:3000/",
  api: "http://localhost:4200/",
  redirect_url: "http://localhost:4200/",
};
